'''
Channel coding module for transmitter and receiver
'''

import numpy
import random
import math
import hamming_db   # Matrices for Hamming codes

__HEADER_CC_LEN = 3
__MAX_PADDING_BITS = 8
__INDEX_BITS = 2
__MAX_PAYLOAD_SIZE_BITS = 18

# Removes '0b' portion of the binary string
def clean_bin_str(bin_str):
    return bin_str[2:]

# Converts a binary string into a numpy array
def numpy_array_from_str(bin_str):
  array = numpy.zeros(len(bin_str))
  for i in range(len(bin_str)):
    if (bin_str[i] == '1'):
      array[i] = 1
  return array

# Since some terms can be 2 after matrix multiplication, mods everything by 2
def binarize_code(coded_bits):
  for i in range(len(coded_bits)):
    coded_bits[i] = coded_bits[i] % 2
  return coded_bits

''' Transmitter side ---------------------------------------------------
'''
def get_frame(databits, cc_len):
    '''
    Perform channel coding to <databits> with Hamming code of n=<cc_len>
    Add header and also apply Hamming code of n=3
    Return the resulted frame (channel-coded header and databits)
    '''
    (n, k, header_index, G) = hamming_db.gen_lookup(cc_len)
    remainder = len(databits) % k
    padding_size = 0
    if (remainder != 0):
      padding_size = k - remainder
      zeros = numpy.zeros(padding_size, dtype=numpy.int).tolist()
      databits = numpy.concatenate([zeros, databits])
    (index, payload) = encode(databits, cc_len)
    header = get_header(payload, index, padding_size)
    frame = numpy.concatenate([header, payload])
    return frame

# When we encode, the resultant code is parity bits then message
# Raw Header Format: index (2 bits), padding_size (8 bits)
# Coded Header Format: index (2 + 4 bits (parity)),
#                      padding_size (8 + 16 bits (parity))
def get_header(payload, index, padding_size):
    '''
    Construct and return header for channel coding information.
    Do not confuse this with header from source module.
    Communication system use layers and attach nested headers from each layers
    '''
    (n, k, header_index, G) = hamming_db.gen_lookup(__HEADER_CC_LEN)
    bin_index_str = clean_bin_str(bin(index))
    while(len(bin_index_str) != __INDEX_BITS):
      bin_index_str = '0' + bin_index_str
    bin_padding_str = clean_bin_str(bin(padding_size))
    while(len(bin_padding_str) != __MAX_PADDING_BITS):
      bin_padding_str = '0' + bin_padding_str
    bin_payload_size_str = clean_bin_str(bin(len(payload)))
    while(len(bin_payload_size_str) != __MAX_PAYLOAD_SIZE_BITS):
      bin_payload_size_str = '0' + bin_payload_size_str

    raw_header = numpy_array_from_str(bin_index_str + bin_padding_str + bin_payload_size_str)
    assert (len(raw_header) ==  __INDEX_BITS + __MAX_PADDING_BITS + __MAX_PAYLOAD_SIZE_BITS)

    # Convert to numpy array
    num_encodings = len(raw_header) / k
    coded_header = numpy.empty(0)
    for i in range(num_encodings):
      sub_array = raw_header[i * k : (i+1) * k]
      coded_sub_array = numpy.dot(sub_array, G)
      coded_header = numpy.concatenate([coded_header, coded_sub_array])
    assert(len(coded_header) % __HEADER_CC_LEN == 0)
    return binarize_code(coded_header)

# When we encode, the resultant code is parity bits then message
def encode(databits, cc_len):
    '''
    Perfrom channel coding to <databits> with Hamming code of n=<cc_len>
    Pad zeros to the databits if needed.
    Return the index of our used code and the channel-coded databits
    '''
    (n, k, index, G) = hamming_db.gen_lookup(cc_len)
    coded_bits = numpy.empty(0)
    num_encodings = len(databits) / k
    numpy_databits = numpy.array(databits)
    for i in range(num_encodings):
      sub_array = numpy_databits[i * k : (i+1) * k]
      coded_sub_array = numpy.dot(sub_array, G)
      coded_bits = numpy.concatenate([coded_bits, coded_sub_array])
    assert(len(coded_bits) % cc_len == 0)
    return index, binarize_code(coded_bits)

''' Receiver side ---------------------------------------------------
'''

def bitstoint(bits):
    bitstring = ""
    for bit in bits:
        if bit == 0:
            bitstring += "0"
        else:
            bitstring += "1"
    return int(bitstring, 2)

def get_databits(recd_bits):
    '''
    Return channel-decoded data bits
    Parse the header and perform channel decoding.
    Note that header is also channel-coded
    '''
    n, k, index, G = hamming_db.gen_lookup(__HEADER_CC_LEN)
    n, k, H = hamming_db.parity_lookup(index)

    totalHeaderSize = __HEADER_CC_LEN * (math.ceil((float(__INDEX_BITS) / k))  + math.ceil((float(__MAX_PADDING_BITS) / k)) + math.ceil((float(__MAX_PAYLOAD_SIZE_BITS) / k)))
    totalHeaderSize = int(totalHeaderSize)
    header = recd_bits[0:totalHeaderSize]
    body = recd_bits[totalHeaderSize:]
    index = -1
    indexBits = numpy.empty(0)
    indexHeaderSize = int(math.ceil((float(__INDEX_BITS) / k)) * n)
    for i in range(indexHeaderSize / n):
        curCodeword = fix_bit_errors(header[i*n:(i+1)*n], H, n, k)
        indexBits = numpy.concatenate([indexBits, curCodeword])
    index = bitstoint(indexBits[0:2])

    padding_zeros = -1
    padding_zeros_string = numpy.empty(0)
    for i in range(int(math.ceil((float(__MAX_PADDING_BITS) / k)))):
        curCodeword = fix_bit_errors(header[indexHeaderSize + (i*n):indexHeaderSize + (i+1)*n], H, n, k)
        padding_zeros_string = numpy.concatenate([padding_zeros_string, curCodeword])
    padding_zeros = bitstoint(padding_zeros_string)

    paddingZerosHeaderSize = int(math.ceil((float(__MAX_PADDING_BITS) / k)) * n)
    payload_size = -1
    payload_size_string = numpy.empty(0)
    for i in range(int(math.ceil((float(__MAX_PAYLOAD_SIZE_BITS) / k)))):
        curCodeword = header[indexHeaderSize + paddingZerosHeaderSize + (i*n):indexHeaderSize + paddingZerosHeaderSize + (i+1)*n]
        curCodeword = fix_bit_errors(curCodeword, H, n, k)
        payload_size_string = numpy.concatenate([payload_size_string, curCodeword])
    payload_size = bitstoint(payload_size_string)

    body = body[0:payload_size]
    recd_bits = decode(body, index)
    recd_bits = recd_bits[padding_zeros:]
    return recd_bits

def decode(coded_bits, index):
    '''
    Decode <coded_bits> with Hamming code which corresponds to <index>
    Return decoded bits
    '''

    n, k, H = hamming_db.parity_lookup(index)
    decoded_bits = numpy.empty(0)
    for i in range(len(coded_bits) / n):
        curCodeword = coded_bits[(i*n):(i*n + n)]
        syndrome = numpy.dot(H, curCodeword)
        curMessage = fix_bit_errors(curCodeword, H, n, k)
        if curMessage == "ERROR":
            print "Error decoding message. Too many bit errors"
            return numpy.empty(0)
        decoded_bits = numpy.concatenate([decoded_bits, curMessage])#curCodeword[0:k]])

    return decoded_bits

def fix_bit_errors(codeword, H, n, k):
    codeword_t = numpy.transpose(codeword)
    syndrome = numpy.dot(H, codeword_t)
    syndrome = binarize_code(syndrome)
    error_exists = False
    for i in range(len(syndrome)):
        if syndrome[i] == 1:
            error_exists = True
            break
    if error_exists:
        bit_fixed = False
        for i in range(len(H[0])):
            match_error = False
            for j in range(len(H)):
                if H[j][i] != syndrome[j]:
                    match_error = True
                    break
            if match_error == False:
                print "Error at index ", i
                flip_bit(codeword, i)
                bit_fixed = True
                break
        if bit_fixed == False:
            return "ERROR"
    return codeword[0:k]

def flip_bit(bit_array, index):
    if bit_array[index] == 0:
        bit_array[index] = 1
    else:
        bit_array[index] = 0
