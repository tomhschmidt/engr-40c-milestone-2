import os

cc_lens = ['3', '7', '15', '31']
noises = ['0.1', '0.125', '0.15', '0.175', '0.2']

for cc_len in cc_lens:
  for noise in noises:
    for i in range(10):
      os.system('time ~/Library/Enthought/Canopy_64bit/User/bin/python2.7 sendrecv.py -f testfiles/random_short.txt -c 1000 -s 256 -q 200 -C ' + cc_len + ' -b -z ' + noise)
