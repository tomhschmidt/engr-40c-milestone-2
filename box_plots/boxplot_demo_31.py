#!/usr/bin/python

#
# Example boxplot code
#
from pylab import *

# Data to Plot
cc3err10 = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
cc3err125 = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
cc3err15 = [0.0, 0.00371287128713, 0.0, 0.0123762376238, 0.0, 0.00618811881188, \
            0.0111386138614, 0.00618811881188, 0.00742574257426, 0.0]
cc3err175 = [0.0148514851485, 0.0, 0.00990099009901, 0.0, 0.00247524752475, \
             0.00618811881188, 0.0, 0.0123762376238, 0.00378787878788, \
             0.00866336633663]
cc3err200 = [0.029702970297, 0.483910891089, 0.0222772277228, 0.029702970297, \
             0.0309405940594, 0.0235148514851, 0.0235148514851, 0.0185643564356, \
             0.0222772277228, 0.0284653465347]


fig = figure()
ax = axes()
hold(True)

# first boxplot pair
bp = boxplot(cc3err10, positions = [1], widths = 0.6)

# second boxplot pair
bp = boxplot(cc3err125, positions = [2], widths = 0.6)

bp = boxplot(cc3err15, positions = [3], widths = 0.6)
bp = boxplot(cc3err175, positions = [4], widths = 0.6)
bp = boxplot(cc3err200, positions = [5], widths = 0.6)

labels=['0.1', '0.125', '0.15', '0.175', '0.2']
xticks(range(1,6),labels, rotation=15)
xlabel('Noise Level')
ylabel('Bit Error Rate')
title('Noise Level VS Bit Error Rate for cc_len=31')

# set axes limits and labels
xlim(0,6)
ylim(0,0.04)

savefig('cclen31.png')
show()
